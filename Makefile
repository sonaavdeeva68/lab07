CC = clang
COPTS = -g -O0 -Wincompatible-pointer-types
BINARY_SRC = dist

all: clean prep compile format
	./$(BINARY_SRC)/main.bin
clean:
	rm -rf $(BINARY_SRC)
prep:
	mkdir $(BINARY_SRC)
compile: main.bin

main.bin: src/main.c
	$(CC) $(COPTS) $< -o ./$(BINARY_SRC)/$@
format:
	doxygen Doxyfile
